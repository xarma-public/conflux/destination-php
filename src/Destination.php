<?php

namespace Xarma\Conflux\Destination;

use Psr\Log\LoggerInterface;

use Xarma\Conflux\Destination\Destination\DestinationObject;

interface Destination
{
    /**
     * Delivers an object to a destination
     *
     * @param DestinationObject $destinationObject
     *
     * @return void
     */
    public function deliver(DestinationObject $destinationObject): void;

    /**
     * @see \Psr\Log\LoggerAwareTrait
     */
    public function setLogger(LoggerInterface $logger): void;
}