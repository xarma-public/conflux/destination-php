<?php

namespace Xarma\Conflux\Destination\Destination;

interface Config
{
    /**
     * Gets a Destination's configuration by key
     *
     * @param string $key
     *
     * @return mixed
     */
    public function get(string $key): mixed;
}