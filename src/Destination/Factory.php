<?php

namespace Xarma\Conflux\Destination\Destination;

use Xarma\Conflux\Destination\Integration;

interface Factory
{
    /**
     * Gets a Destination for the given Destination Config
     *
     * @param Config $config
     *
     * @return Integration
     */
    public function get(Config $config): Integration;
}