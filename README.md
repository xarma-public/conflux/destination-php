# Conflux Destination

A collection of interfaces used to define data destinations for [Xarma Conflux](https://xarma.co/conflux).

## Usage

```bash
$ composer require xarma/conflux-destination
```
